from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])

    message = ""
    
    if dividend%2 == 0:
        message += f"{dividend} is even.\n"
    else:
        message += f"{dividend} is odd.\n"
    
    if dividend%4 == 0:
        message += f"{dividend} is multiple of 4.\n"
    
    if dividend%divisor == 0:
        message += f"{dividend} is a multiple of {divisor}."
    
    return message

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = int(request.form['grade'])

    if grade > 100 or grade < 0:
        return f'wrong score: {grade}, {name} please re-enter'
    
    if grade >= 95:
        return f"{name}'s score {grade}--> you get A"
    elif 95 > grade and grade >= 80:
        return f"{name}'s score {grade}--> you get B"
    elif 80 > grade and grade >= 70:
        return f"{name}'s score {grade}--> you get C"
    elif 70 > grade and grade >= 60:
        return f"{name}'s score {grade}--> you get D"
    else:
        return f"{name}'s score {grade}--> you get F"

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    import math
    value = int(request.form['value'])

    if value < 0:
        return f"{value} cannot be square rooted"
    
    return f"the square root of {value} is {math.sqrt(value)}"

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    import random
    
    counter = 1
    number = int(request.form['number'])
    
    randomNum = random.randrange(1, 100)

    while number != randomNum:
        randomNum = random.randrange(1, 100)
        counter += 1
    
    return f"number matched at {number} and it took {counter} times"

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    limit = int(request.form['number'])
    
    following_num = 1
    second_num = 1
    if limit <= 0:
        return f"{limit}: error... limit is <=0"
    
    sequence = f"{limit}: 0, 1, "

    while following_num <= limit:
        sequence += f"{following_num}, "
        first_num = second_num
        second_num = following_num
        following_num = first_num + second_num
    
    return sequence  